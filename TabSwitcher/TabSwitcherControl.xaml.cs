﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TabSwitcher
{
    /// <summary>
    /// Interaction logic for TabSwitcherControl.xaml
    /// </summary>
    public partial class TabSwitcherControl : UserControl
    {
        private bool previousButtonHidden;
        private bool nextButtonHidden;

        public TabSwitcherControl()
        {
            InitializeComponent();
        }

        public bool IsHidePreviousButton
        {
            get { return previousButtonHidden; }
            set
            {
                previousButtonHidden = value;
                SetButtons();
            }
        }

        public bool IsHideNextButton
        {
            get { return nextButtonHidden; }
            set
            {
                nextButtonHidden = value;
                SetButtons();
            }
        }

        private void SetButtons()
        {
            PreviousButton.Visibility = previousButtonHidden ? Visibility.Collapsed : Visibility.Visible;
            NextButton.Visibility = nextButtonHidden ? Visibility.Collapsed : Visibility.Visible;
            if (previousButtonHidden || nextButtonHidden)
            {
                PreviousButton.Width = 230;
                NextButton.Width = 230;
            }
            else
            {
                PreviousButton.Width = 115;
                NextButton.Width = 115;
            }
        }

        public RoutedEventHandler NextButtonClick { get; set; }

        public RoutedEventHandler PreviousButtonClick { get; set; }

        private void PreviousButton_Click(object sender, RoutedEventArgs e)
        {
            PreviousButtonClick?.Invoke(sender, e);
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            NextButtonClick?.Invoke(sender, e);
        }
    }
}
