﻿using CodePassword;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace CodePassword.Tests
{
    [TestClass]
    public class PasswordTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            Debug.WriteLine("TestInitialize");
        }

        [TestCleanup]
        public void TestCleanup()
        {
            Debug.WriteLine("TestCleanup");
        }

        [TestMethod]
        public void EncodePassword_abc_bcd()
        {
            // arrange
            string psw = "abc";
            string expectedPsw = "bcd";
            // act
            string actualPsw = Password.EncodePassword(psw);
            //assert
            Assert.AreEqual(expectedPsw, actualPsw);
        }

        [TestMethod]
        public void EncodePassword_empty_empty()
        {
            // arrange
            string psw = "";
            string expectedPsw = "";
            // act
            string actualPsw = Password.EncodePassword(psw);
            //assert
            Assert.AreEqual(expectedPsw, actualPsw);
        }

        [TestMethod()]
        public void DecodePassword_bcd_abc()
        {
            // arrange
            string encodedPsw = "bcd";
            string expectedPsw = "abc";
            // act
            Debug.WriteLine($"Decode password from <{encodedPsw}>");
            string actualPsw = Password.DecodePassword(encodedPsw);
            //assert
            Assert.AreEqual(expectedPsw, actualPsw, "Decode wrong");
        }

        [TestMethod()]
        public void DecodePassword_empty_empty()
        {
            // arrange
            string encodedPsw = "";
            string expectedPsw = "";
            // act
            Debug.WriteLine($"Decode password from <{encodedPsw}>");
            string actualPsw = Password.EncodePassword(encodedPsw);
            //assert
            Assert.AreEqual(expectedPsw, actualPsw, "Decode wrong");
        }
    }
}
