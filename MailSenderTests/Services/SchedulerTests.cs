﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MailSender.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace MailSender.Services.Tests
{
    [TestClass()]
    public class SchedulerTests
    {
        private static Scheduler sc;
        private static TimeSpan ts;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Debug.WriteLine("Test Initialize");
            ts = new TimeSpan(); // возвращаем в случае ошибочно введенного времени
            sc = new Scheduler();
            sc.EmailTextByDates = new Dictionary<DateTime, string>()
            {
                { new DateTime(2016, 12, 24, 22, 0, 0), "text1" },
                { new DateTime(2016, 12, 24, 22, 30, 0), "text2" },
                { new DateTime(2016, 12, 24, 23, 0, 0), "text3" }
            };
        }

        [TestMethod()]
        public void TimeTick_Dictionare_correct()
        {
            DateTime dt1 = new DateTime(2016, 12, 24, 22, 0, 0);
            DateTime dt2 = new DateTime(2016, 12, 24, 22, 30, 0);
            DateTime dt3 = new DateTime(2016, 12, 24, 23, 0, 0);

            if (sc.EmailTextByDates.Keys.First<DateTime>().ToShortTimeString() == dt1.ToShortTimeString())
            {
                Debug.WriteLine("Body " + sc.EmailTextByDates[sc.EmailTextByDates.Keys.First<DateTime>()]);
                Debug.WriteLine("Subject " + $"Рассылка от {sc.EmailTextByDates.Keys.First<DateTime>().ToShortDateString()}  {sc.EmailTextByDates.Keys.First<DateTime>().ToShortTimeString()}");
                sc.EmailTextByDates.Remove(sc.EmailTextByDates.Keys.First<DateTime>());
            }

            if (sc.EmailTextByDates.Keys.First<DateTime>().ToShortTimeString() == dt2.ToShortTimeString())
            {
                Debug.WriteLine("Body " + sc.EmailTextByDates[sc.EmailTextByDates.Keys.First<DateTime>()]);
                Debug.WriteLine("Subject " + $"Рассылка от {sc.EmailTextByDates.Keys.First<DateTime>().ToShortDateString()}  {sc.EmailTextByDates.Keys.First<DateTime>().ToShortTimeString()}");
                sc.EmailTextByDates.Remove(sc.EmailTextByDates.Keys.First<DateTime>());
            }

            if (sc.EmailTextByDates.Keys.First<DateTime>().ToShortTimeString() == dt3.ToShortTimeString())
            {
                Debug.WriteLine("Body " + sc.EmailTextByDates[sc.EmailTextByDates.Keys.First<DateTime>()]);
                Debug.WriteLine("Subject " + $"Рассылка от {sc.EmailTextByDates.Keys.First<DateTime>().ToShortDateString()}  {sc.EmailTextByDates.Keys.First<DateTime>().ToShortTimeString()}");

                sc.EmailTextByDates.Remove(sc.EmailTextByDates.Keys.First<DateTime>());
            }

            Assert.AreEqual(0, sc.EmailTextByDates.Count);
        }

        [TestMethod()]
        public void ToTimeSpan_empty_ts()
        {
            string time = "";
            TimeSpan actualTime = Scheduler.ToTimeSpan(time);
            Assert.AreEqual(ts, actualTime);
        }

        [TestMethod()]
        public void ToTimeSpan_sdf_ts()
        {
            string time = "sdf";
            TimeSpan actualTime = Scheduler.ToTimeSpan(time);
            Assert.AreEqual(ts, actualTime);
        }

        [TestMethod()]
        public void ToTimeSpan_correctTime_Equal()
        {
            string time = "12:12";
            TimeSpan correctTime = new TimeSpan(12, 12, 0);
            TimeSpan actualTime = Scheduler.ToTimeSpan(time);
            Assert.AreEqual(correctTime, actualTime);
        }

        [TestMethod()]
        public void ToTimeSpan_inCorrectHour_ts()
        {
            string time = "25:12";
            TimeSpan actualTime = Scheduler.ToTimeSpan(time);
            Assert.AreEqual(ts, actualTime);
        }

        [TestMethod()]
        public void ToTimeSpan_inCorrectMin_ts()
        {
            string time = "12:65";
            TimeSpan actualTime = Scheduler.ToTimeSpan(time);
            Assert.AreEqual(ts, actualTime);
        }
    }
}