﻿using MailLib;
using MailSender.MailsAndSendersDataSetTableAdapters;
using MailSender.Services;
using MailSender.Services.Repositories;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MailSender
{
    /// <summary>
    /// Interaction logic for WpfMailSender.xaml
    /// </summary>
    public partial class WpfMailSender : Window
    {
        private DBService db = new DBService();

        public WpfMailSender()
        {
            InitializeComponent();

            TabSwitcher.NextButtonClick += TabSwitcher_NextButtonClick;

            SenderCombo.ItemsSource = Variables.Senders;
            SenderCombo.DisplayMemberPath = "Key";
            SenderCombo.SelectedValuePath = "Value";
            MailsGrid.ItemsSource = db.GetMails().ToList();

            SmtpCombo.ItemsSource = Variables.SmtpServers;
            SmtpCombo.DisplayMemberPath = "Key";
            SmtpCombo.SelectedValuePath = "Value";
        }

        private void TabSwitcher_NextButtonClick(object sender, RoutedEventArgs e)
        {
            TabControl.SelectedIndex = 1;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (db != null)
            {
                db.Dispose();
                db = null;
            }
        }

        private void CloseMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SendAtOnceButton_Click(object sender, RoutedEventArgs e)
        {
            string login = SenderCombo.Text;
            string password = SenderCombo.SelectedValue.ToString();
            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Выберите отправителя");
                return;
            }
            string smtpServer = SmtpCombo.Text;
            if (string.IsNullOrEmpty(smtpServer))
            {
                MessageBox.Show("Выберите smtp-сервер");
                return;
            }
            int smtpPort = (int)SmtpCombo.SelectedValue;
            TextRange textRange = new TextRange(MailText.Document.ContentStart, MailText.Document.ContentEnd);
            string mailText = textRange.Text;
            if (string.IsNullOrEmpty(mailText))
            {
                MessageBox.Show("Заполните текст письма");
                EditorTab.IsSelected = true;
                return;
            }
            EmailSendService emailSender = new EmailSendService(login, password);
            emailSender.Body = mailText;
            emailSender.SendMails(((IEnumerable<Mail>)MailsGrid.ItemsSource).Select(m => m.Email));

        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            string login = SenderCombo.Text;
            string password = SenderCombo.SelectedValue.ToString();
            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Выберите отправителя");
                return;
            }
            Scheduler sc = new Scheduler();
            TimeSpan sendTime = Scheduler.ToTimeSpan(TimePicker.Text);
            if (sendTime == new TimeSpan())
            {
                MessageBox.Show("Некорректный формат даты");
                return;
            }
            DateTime sendDateTime = (ScheduleDateTimes.SelectedDate ?? DateTime.Today).Add(sendTime);
            if (sendDateTime < DateTime.Now)
            {
                MessageBox.Show("Дата и время отправки писем не могут быть раньше, чем настоящее время");
                return;
            }
            TextRange textRange = new TextRange(MailText.Document.ContentStart, MailText.Document.ContentEnd);
            string mailText = textRange.Text;
            if (string.IsNullOrEmpty(mailText))
            {
                MessageBox.Show("Заполните текст письма");
                EditorTab.IsSelected = true;
                return;
            }
            EmailSendService emailSender = new EmailSendService(login, password);
            emailSender.Body = mailText;
            sc.SendEmails(sendDateTime, emailSender, (IEnumerable<Mail>)MailsGrid.ItemsSource);
        }

        private void ScheduleButton_Click(object sender, RoutedEventArgs e)
        {
            ScheduleTab.IsSelected = true;
        }

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            ReportDataSource dataSource = new ReportDataSource();
            MailsAndSendersDataSet ds = new MailsAndSendersDataSet();
            ds.BeginInit();
            try
            {
                dataSource.Name = "Mails";
                dataSource.Value = ds.Mails;
                ReportViewer.LocalReport.DataSources.Add(dataSource);
                ReportViewer.LocalReport.ReportPath = "../../Reports/Report.rdlc";
            }
            finally
            {
                ds.EndInit();
            }
            MailsTableAdapter mailsTableAdapter = new MailsTableAdapter { ClearBeforeFill = true };
            mailsTableAdapter.Fill(ds.Mails);
            ReportViewer.RefreshReport();
        }
    }
}
