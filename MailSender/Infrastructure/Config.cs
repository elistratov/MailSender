﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailSender.Infrastructure
{
    public static class Config
    {
        public static string MyEmail
        {
            get { return Properties.Settings.Default.MyEmail; }
        }

        public static string SmtpHost
        {
            get { return Properties.Settings.Default.SmtpHost; }
        }

        public static int SmtpPort
        {
            get { return Properties.Settings.Default.SmtpPort; }
        }
    }
}
