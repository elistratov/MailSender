﻿using CodePassword;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailSender.Services.Repositories
{
    public static class Variables
    {
        private static Dictionary<string, string> senders = new Dictionary<string, string>()
        {
            { "​test@ksergey.ru﻿", Password.DecodePassword("qwerty2018﻿") },
            { "spam1@ksergey.ru", Password.DecodePassword("qwerty2018﻿") },
            { "cdscds@ksergey.ru﻿", Password.DecodePassword("qwerty2018﻿") }
        };

        private static Dictionary<string, int> smtpServers = new Dictionary<string, int>()
        {
            { "smtp.yandex.ru﻿", 25 }
        };

        public static Dictionary<string, string> Senders
        {
            get { return senders; }
        }

        public static Dictionary<string, int> SmtpServers
        {
            get { return smtpServers; }
        }
    }
}
