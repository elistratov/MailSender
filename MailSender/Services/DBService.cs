﻿using MailSender.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailSender.Services
{
    /// <summary>
    /// Работа с БД
    /// </summary>
    public class DBService : IDisposable
    {
        private MailsContext ctx;

        public DBService()
        {
            ctx = new MailsContext();
        }

        ~DBService()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (ctx != null)
                {
                    ctx.Dispose();
                    ctx = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Список сохраненных писем
        /// </summary>
        /// <returns>Сохраненные письма</returns>
        public IQueryable<Mail> GetMails()
        {
            return from m in ctx.Mails
                   select m;
        }
    }
}
