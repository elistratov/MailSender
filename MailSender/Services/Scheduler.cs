﻿using MailLib;
using MailSender.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace MailSender.Services
{
    public class Scheduler
    {
        private DispatcherTimer timer = new DispatcherTimer(); // таймер 
        private EmailSendService emailSender; // экземпляр класса, отвечающего за отправку писем
        private DateTime sendDate; // дата и время отправки
        private List<Mail> mails; // коллекция email'ов адресатов

        Dictionary<DateTime, string> emailTextByDates = new Dictionary<DateTime, string>();
        public Dictionary<DateTime, string> EmailTextByDates
        {
            get { return emailTextByDates; }
            set
            {
                emailTextByDates = value;
                emailTextByDates = emailTextByDates.OrderBy(pair => pair.Key).ToDictionary(pair => pair.Key, pair => pair.Value);
            }
        }

        public static TimeSpan ToTimeSpan(string time)
        {
            TimeSpan ts = new TimeSpan();
            try
            {
                ts = TimeSpan.Parse(time);
            }
            catch
            { }
            return ts;
        }

        public void SendEmails(DateTime dtSend, EmailSendService emailSender, IEnumerable<Mail> mails)
        {
            this.emailSender = emailSender; // Экземпляр класса, отвечающего за отправку писем
            this.sendDate = dtSend;
            this.mails = mails.ToList();
            timer.Tick += Timer_Tick;
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            //if (sendDate >= DateTime.Now)
            //{
            //    emailSender.SendMails(mails.Select(m => m.Email));
            //    timer.Stop();
            //    MessageBox.Show("Письма отправлены.");
            //}
            if (emailTextByDates.Count == 0)
            {
                timer.Stop();
                MessageBox.Show("Письма отправлены.");
            }
            else if (emailTextByDates.Keys.First<DateTime>().ToShortTimeString() == DateTime.Now.ToShortTimeString())
            {
                emailSender.Body = emailTextByDates[emailTextByDates.Keys.First<DateTime>()];
                emailSender.Subject = $"Рассылка от {emailTextByDates.Keys.First<DateTime>().ToShortTimeString()} ";
                emailSender.SendMails(mails.Select(m => m.Email));
                emailTextByDates.Remove(emailTextByDates.Keys.First<DateTime>());
            }

        }
    }
}
