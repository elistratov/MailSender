﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTpl
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrixMultiplication();
            //ProcessFiles("Data");

            Console.Write("Press any key...");
            Console.ReadKey();
        }

        static void MatrixMultiplication()
        {
            int[,] M1 = new int[100, 100];
            int[,] M2 = new int[100, 100];
            int[] result = new int[100];

            Random rnd = new Random();
            for (int i = 0; i < 100; ++i)
            {
                for (int j = 0; j < 100; ++j)
                {
                    M1[i, j] = rnd.Next(11);
                    M2[i, j] = rnd.Next(11);
                }
            }

            Console.WriteLine("Синхронное перемножение матриц:");
            for (int i = 0; i < 100; ++i)
            {
                result[i] = 0;
                for (int j = 0; j < 100; ++j)
                {
                    result[i] += M1[i, j] * M2[j, i];
                }
            }
            PrintResult(result);

            Console.WriteLine("Параллельное перемножение матриц:");
            Parallel.For(0, 100, i =>
                {
                    result[i] = 0;
                    for (int j = 0; j < 100; ++j)
                        result[i] += M1[i, j] * M2[j, i];
                });
            PrintResult(result);
        }

        static void PrintResult(int[] r)
        {
            for (int i = 0; i < r.Length; ++i)
                Console.Write(" {0}", r[i]);
            Console.WriteLine();
        }

        static void ProcessFiles(string directoryName)
        {
            DirectoryInfo di = new DirectoryInfo(directoryName);
            if (!di.Exists)
            {
                Console.WriteLine("Директория <{0}> не существует", directoryName);
                return;
            }

            using (var f = File.Create("result.dat"))
            {
            }

            //foreach (FileInfo file in di.GetFiles())
            //{
            //    CalcFile(file);
            //}
            Parallel.ForEach(di.GetFiles(), CalcFile);
        }

        private static object guard = new object();

        static void CalcFile(FileInfo file)
        {
            string text = File.ReadAllText(file.FullName);
            if (string.IsNullOrEmpty(text))
                return;
            string[] items = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (items.Length < 3)
                return;
            try
            {
                int op = int.Parse(items[0]);
                double val1 = double.Parse(items[1]);
                double val2 = double.Parse(items[2]);
                double result = (op == 1) ? val1 * val2 : val1 / val2;

                lock (guard)
                {
                    File.AppendAllText("result.dat", result.ToString() + Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка: " + ex.Message);
                return;
            }
        }
    }
}
