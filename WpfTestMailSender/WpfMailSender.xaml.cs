﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfTestMailSender.Infrastructure;
using WpfTestMailSender.Services;

namespace WpfTestMailSender
{
    /// <summary>
    /// Interaction logic for WpfMailSender.xaml
    /// </summary>
    public partial class WpfMailSender : Window
    {
        public WpfMailSender()
        {
            InitializeComponent();
        }

        private void SendEmailButton_Click(object sender, RoutedEventArgs e)
        {
            List<string> mails = new List<string> { "testEmail@gmail.com", "email@yandex.ru" }; // Список email'ов кому мы отправляем письмо
            string password = PasswordBox.Password;
            EmailSendService emailSender = new EmailSendService(Config.MyEmail, new NetworkCredential(Config.MyEmail, password));

            foreach (var mail in mails)
            {
                try
                {
                    emailSender.SendMail(SubjectText.Text, BodyText.Text, mail);
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Невозможно отправить письмо " + ex.ToString());
                    var alert = new AlertWindow() { Owner = this };
                    alert.ErrorMessage.Text = "Невозможно отправить письмо " + ex.ToString();
                    alert.ShowDialog();
                }
            }
            //MessageBox.Show("Работа завершена.");
            var w = new SendEndWindow() { Owner = this };
            w.Show();
        }
    }
}
