﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using WpfTestMailSender.Infrastructure;

namespace WpfTestMailSender.Services
{
    public class EmailSendService
    {
        private string from;
        private NetworkCredential credentials;

        public EmailSendService(string from, NetworkCredential credentials)
        {
            this.from = from;
            this.credentials = credentials;
        }

        public void SendMail(string subject, string body, string to)
        {
            using (MailMessage mm = new MailMessage(from, to))
            {
                // Формируем письмо
                mm.Subject = subject;   // Заголовок письма
                mm.Body = body;         // Тело письма
                mm.IsBodyHtml = false;  // Не используем html в теле письма
                // Авторизуемся на smtp-сервере и отправляем письмо
                // Оператор using гарантирует вызов метода Dispose, даже если при вызове 
                // методов в объекте происходит исключение.
                using (var sc = new SmtpClient(Config.SmtpHost, Config.SmtpPort))
                {
                    sc.EnableSsl = true;
                    sc.Credentials = credentials;
                    sc.Send(mm);
                }
            }
        }
    }
}
