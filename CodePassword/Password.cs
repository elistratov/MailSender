﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodePassword
{
    public static class Password
    {
        /// <summary>
        /// На вход подаём зашифрованный пароль, на выходе получаем пароль для email
        /// </summary>
        /// <param name="password">Зашифрованный пароль</param>
        /// <returns></returns>
        public static string DecodePassword(string password)
        {
            string psw = "";
            foreach (char a in password)
            {
                char ch = a;
                ch--;
                psw += ch;
            }
            return psw;
        }

        /// <summary>
        /// На вход подаем пароль, на выходе получаем зашифрованный пароль
        /// </summary> 
        /// <param name="password">Пароль</param> 
        /// <returns></returns>
        public static string EncodePassword(string password)
        {
            string code = "";
            foreach (char a in password)
            {
                char ch = a;
                ch++;
                code += ch;
            }
            return code;
        }
    }
}
