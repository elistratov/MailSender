﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MailLib
{
    /// <summary>
    /// Служба почтовой рассылки
    /// </summary>
    public class EmailSendService
    {
        private string from;
        private NetworkCredential credentials;

        /// <summary>
        /// Служба почтовой рассылки
        /// </summary>
        /// <param name="login">От кого</param>
        /// <param name="credentials">Логин и пароль для авторизации на почтовом сервере</param>
        public EmailSendService(string login, string password)
        {
            this.from = login;
            this.credentials = new NetworkCredential(login, password);
            SmtpHost = "smtp.yandex.ru";
            SmtpPort = 25;
        }

        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

        /// <summary>
        /// Тема письма
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Содержание письма
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Отправка письма
        /// </summary>
        /// <param name="to">Кому (адресат письма)</param>
        public void SendMail(string to)
        {
            using (MailMessage mm = new MailMessage(from, to))
            {
                // Формируем письмо
                mm.Subject = Subject;   // Заголовок письма
                mm.Body = Body;         // Тело письма
                mm.IsBodyHtml = false;  // Не используем html в теле письма
                // Авторизуемся на smtp-сервере и отправляем письмо
                // Оператор using гарантирует вызов метода Dispose, даже если при вызове 
                // методов в объекте происходит исключение.
                using (var sc = new SmtpClient(SmtpHost, SmtpPort))
                {
                    sc.EnableSsl = true;
                    sc.Credentials = credentials;
                    sc.Send(mm);
                }
            }
        }

        /// <summary>
        /// Отправка письма с подавлением возможных ошибок
        /// </summary>
        /// <param name="to">Кому (адресат письма)</param>
        public void TrySendMail(string to)
        {
            try
            {
                SendMail(to);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Невозможно отправить письмо\n" + ex.Message);
            }
        }

        public void SendMails(IEnumerable<string> addresses)
        {
            foreach (var email in addresses)
                TrySendMail(email);
        }
            
    }
}
